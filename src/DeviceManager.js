const devices = {};
import AndroidDevice from "./AndroidDevice";

export default {
    getAndroidDevice: (serial) => {
        if (!devices[serial]) {
            devices[serial] = new AndroidDevice(serial);
        }

        return devices[serial];
    }
};