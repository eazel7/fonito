import App from './App.svelte';

import DevicesStore from "./DevicesStore";

const app = new App({
    target: document.body,
    props: {
        devices: DevicesStore
    }
});

export default app;