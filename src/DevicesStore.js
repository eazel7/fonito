const { writable } = require("svelte/store");

import ADBDevicesMonitor from "./ADBDevicesMonitor";
import DevicesManager from "./DeviceManager";
const waitTime = 1000;

const checkDevices = (store) => {
    ADBDevicesMonitor.listDevices().then((serials) => {
        const devices = serials.map(serial => DevicesManager.getAndroidDevice(serial));

        store.update(v => devices);

        setTimeout(() => checkDevices(store), waitTime);
    }, err => {
        console.error('error retrieving ADB devices');
        console.error(err);
    
        setTimeout(() => checkDevices(store), waitTime);
    })
};

const store = writable([]);

checkDevices(store);

export default store;