const exec = require('child_process').exec;

let adbCommand;

if (require('os').platform() == 'darwin') {
    adbCommand = '/usr/local/bin/adb';
} else {
    adbCommand = 'adb';
}

export default {
    listDevices: () => {
        return new Promise((resolve, reject) => {
            exec(`${adbCommand} devices`, (error, stdout, stderr) => {
                if (error) return reject(error);
                if (!!stderr) return reject(stderr);
                
                let lines = stdout.replace('\n\r', '\n').split('\n');

                lines = lines.slice(lines.indexOf("List of devices attached") + 1, lines.length).map(l => l.split('\t')[0]).filter(v => !!v);

                resolve(lines);
            });
        });
    }
}