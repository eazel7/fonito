const exec = require('child_process').exec;
const macterm = `#!/bin/sh

echo '
on run argv
  if length of argv is equal to 0
    set command to ""
  else
    set command to item 1 of argv
  end if

  if length of argv is greater than 1
    set profile to item 2 of argv
    runWithProfile(command, profile)
  else
    runSimple(command)
  end if
end run

on runSimple(command)
  tell application "Terminal"
    activate
    set newTab to do script(command)
  end tell
  return newTab
end runSimple

on runWithProfile(command, profile)
  set newTab to runSimple(command)
  tell application "Terminal" to set current settings of newTab to (first settings set whose name is profile)
end runWithProfile
' | osascript - "$@" > /dev/null
`;

const agnostincExec = (command) => {
    console.log(command);
    return new Promise((resolve, reject) => {
        if (require('os').platform() == 'darwin') {
            exec(`mktemp`, (error, stdout, stderr) => {
                const mactermSh = stdout.replace('\n', '').replace('\r', '');

                console.log(mactermSh);

                require('fs').writeFileSync(mactermSh, `#! /bin/sh
                
                ${command}`);
                require('fs').chmodSync(mactermSh, '766');

                exec(mactermSh, (error, stdout, stderr) => {
                    if (error) return reject(error);
                    if (!!stderr) return reject(stderr);

                    resolve(stdout);
                });
            });
        } else {
            exec(command, (error, stdout, stderr) => {
                if (error) return reject(error);
                if (!!stderr) return reject(stderr);

                resolve();
            });
        }
    })
}

let adbCommand;

if (require('os').platform() == 'darwin') {
    adbCommand = '/usr/local/bin/adb';
} else {
    adbCommand = 'adb';
}
export default class AndroidDevice {
    constructor(serial) {
        this.serial = serial;
    }

    get name() {
        return this.serial;
    }

    installApk(path) {
        return new Promise((resolve, reject) => {
            agnostincExec(`${adbCommand} -s ${this.serial} install \'${path}\'`, (error, stdout, stderr) => {
                if (error) {
                    console.error(error);
                    return reject(error);
                }
                if (stderr) {
                    console.error(stderr);
                    return reject(stderr);
                }

                console.log(stdout);

                resolve();
            });
        });
    }

    availableTasks() {
        return [{
            name: 'Display screen',
            run: () => {
                if (require('os').platform == 'darwin') {
                    exec(`mktemp`, (error, stdout, stderr) => {
                        const mactermSh = stdout.replace('\n', '').replace('\r', '');
                        require('fs').writeFileSync(mactermSh, `#! /bin/sh
                        
                        PATH=$PATH:/usr/local/bin
                        /usr/local/bin/scrcpy -s ${this.serial}`);
                        require('fs').chmodSync(mactermSh, '766');

                        exec(mactermSh, (error, stdout, stderr) => {
                            console.log(stdout);
                            console.error(error);
                            console.error(stderr);
                        });
                    });
                } else {
                    exec(`scrcpy -s ${this.serial}`)
                }
            }
        }, {
            name: 'Open shell',
            run: () => {
                if (require('os').platform == 'darwin') {
                    exec(`mktemp`, (error, stdout, stderr) => {
                        const mactermSh = stdout.replace('\n', '').replace('\r', '');
                        require('fs').writeFileSync(mactermSh, macterm);
                        require('fs').chmodSync(mactermSh, '766');

                        exec(`${mactermSh} "${adbCommand} -s ${this.serial} shell"`, (error, stdout, stderr) => {
                            console.log(stdout);
                            console.error(error);
                            console.error(stderr);
                        });
                    });
                }
                else {
                    exec(`gnome-terminal -- ${adbCommand} -s ${this.serial} shell`);
                }
            }
        }, {
            name: 'Restart',
            run: () => {
                agnostincExec(`${adbCommand} -s ${this.serial} reboot`);
            }
        }]
    }
}