const {
    app, BrowserWindow } = require("electron");

app.whenReady().then(() => {
    const mainWindow = new BrowserWindow({
        width: 1200,
        height: 800,
        webPreferences: {
            nodeIntegration: true,
        },
    });

    mainWindow.loadFile("./public/index.html");
    // mainWindow.webContents.openDevTools() 
});

app.on("window-all-closed", () => {
    app.quit();
});